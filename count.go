package ep

import "gorm.io/gorm"

type CountStruct struct {
	// db表
	db *gorm.DB

	table string

	model interface{}

	where      string //统计的条件
	whereArgs  []interface{}
	countValue int64 //统计的值
}

// Count 统计次数
func Count(where string, whereArgs ...interface{}) *CountStruct {
	return &CountStruct{where: where, whereArgs: whereArgs}
}

func (c *CountStruct) Db(db *gorm.DB) *CountStruct {
	c.db = db
	return c
}

func (c *CountStruct) Table(tableName string) *CountStruct {
	c.table = tableName
	return c
}

func (c *CountStruct) Model(model interface{}) *CountStruct {
	c.model = model
	return c
}

func (c *CountStruct) count() *CountStruct {
	var query *gorm.DB

	if c.table != "" {
		query = c.db.Table(c.table)
	} else {
		query = c.db.Model(c.model)
	}

	query.Where(c.where, c.whereArgs...).Count(&c.countValue)
	return c
}

func (c *CountStruct) Val() int64 {
	return c.count().countValue
}

func (c *CountStruct) IsRepeat() bool {
	return c.count().countValue != 0
}
